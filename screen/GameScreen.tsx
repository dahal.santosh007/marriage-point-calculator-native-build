import React, { useState,useEffect } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Keyboard, SafeAreaView, TextInput} from 'react-native';
import axios from 'axios';
import Ionicons from '@expo/vector-icons/build/Ionicons';

enum StatusType{
    unseen='unseen',
    seen='seen',
    winner='winner',
    doublee_winner='doublee_winner'
}

const DismissalKeyboard = ({children}:any):JSX.Element=>{
    return <TouchableWithoutFeedback onPress = {()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
};

const GameScreen = ({route}:any) => {
    const [playersName, setPlayersName] = useState([]);
    const [playersPoint, setPlayersPoint] = useState<string[]>([]);
    const [playersPointNum, setPlayersPointNum] = useState<number[]>([]);
    const [playerStatus, setPlayerStatus] = useState<StatusType[]>([]);

    useEffect(() => {
        (async ()=>{
            try {
                const res = await axios.get(`http://192.168.0.85:5000/api/v1/storeGameInfo/ba4cb42d-df9e-4cac-af5f-1836ace3945e`);
                const {data} = res.data;
                setPlayersName(data.playersName);
                let testarr= new Array(data.playersName.length).fill('unseen');
                setPlayerStatus(testarr)
            } catch (error) {
                console.log(error);
            }
        }
        )();
    }, [])

    const onPlayerPointsSet = (index:number,text:string) =>{
        playersPoint[index] = text
        playersPointNum[index] = isNaN(parseInt(text))?0:parseInt(text);
        setPlayersPoint(arr=>[...arr])
        setPlayersPointNum(arr => [...arr])
    }

    const iconType = (playerStatusString:StatusType)=>{
        switch(playerStatusString){
            case StatusType.unseen:
                return <Ionicons size={24} name="eye-off-outline"/>
            case StatusType.seen:
                return <Ionicons size={24} name="eye-outline"/>
            case StatusType.winner:
                return <Ionicons size={24} name="trophy-outline"/>
            case StatusType.doublee_winner:
                return <Ionicons size={24} name="checkmark-done-circle-outline"/>
            default:
                return <Ionicons size={24} name="eye-off-outline"/>
        }
    }

    return (
        <DismissalKeyboard>
            <SafeAreaView style = {styles.main_screen_container}>
                <View style={styles.main_screen_padding}>
                {
                    playersName.map((playerName,index)=>{
                        const playerIndexVal = playersName.indexOf(playerName);
                        return (
                            <View key={index} style={styles.name_point_input}>
                                <View style={styles.player_name}>
                                    <Text style={styles.player_name_size}>{playerName}</Text>
                                </View>
                                <View style={styles.player_status}>
                                    { iconType(playerStatus[playerIndexVal])}
                                </View>
                                <TextInput
                                    keyboardType='numeric'
                                    style={styles.input}
                                    placeholder='Enter points'
                                    placeholderTextColor="#aaaaaa"
                                    onChangeText={(text) => onPlayerPointsSet(playerIndexVal,text)}
                                    value={playersPoint[playerIndexVal]}
                                    underlineColorAndroid="transparent"
                                    autoCapitalize="none"
                                />
                            </View>
                        )
                    })
                }
                </View>
            </SafeAreaView>
        </DismissalKeyboard>
    )
}

export default GameScreen

const styles = StyleSheet.create({
    main_screen_container:{
        flex: 1,
        backgroundColor: '#008080',
    },
    main_screen_padding:{
        padding: 20,
    },
    name_point_input:{
        flexDirection:'row',
    },
    input: {
        height: 48,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 16,
        flex:2
    },
    player_name:{
        flex:2,      
        justifyContent:'center',
        alignItems:'flex-start'
    },
    player_status:{
        flex:1,      
        justifyContent:'center',
        alignItems:'flex-start'
    },
    player_name_size:{
        fontSize:20
    }
})