import React, { useEffect, useState } from 'react';
import { View, 
        Text, 
        StyleSheet, 
        SafeAreaView, 
        TextInput, 
        TouchableWithoutFeedback, 
        Keyboard,
        TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import uuid from 'react-native-uuid';
import axios from 'axios';

const DismissalKeyboard = ({children}:any):JSX.Element=>{
    return <TouchableWithoutFeedback onPress = {()=>Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
};

const DynaminInput = (props:any):JSX.Element =>{
    return <TextInput
                key={props.id}
                style={styles.input}
                placeholder='Enter player name'
                placeholderTextColor="#aaaaaa"
                onChangeText={(text) => props.handleAddPlayerName(props.id,text)}
                value={props.name}
                underlineColorAndroid="transparent"
                autoCapitalize="words"/>
}

const HomeScreen = ({navigation}:any):JSX.Element=> {
    const [numOfPlayer, setNumOfPlayer] = useState('');
    const [numValue, setNumValue] = useState(0);
    const [playersName, setPlayersName] = useState<string[]>([]);

    useEffect(() => {
        setNumValue(parseInt(numOfPlayer || '0')); 
    }, [numOfPlayer,playersName]);

    const handleAddPlayerName = (id:number,name:string) =>{
        playersName[id]=name
        setPlayersName(arr=>[...arr])
    }

    const createDynamicInput = () =>{
        let playerNameInput = []
        for (let i = 0 ; i < numValue; i++){
            playerNameInput.push(<DynaminInput id = {i} handleAddPlayerName = {handleAddPlayerName} name = {playersName[i]}/>)
        }
        return playerNameInput;
    }

    const onSetNumOfPlayer=(val:string)=>{
        if (parseInt(val) < 7 && parseInt(val) > 1){
            setNumOfPlayer(val);
        }else{
            setNumOfPlayer('');
        }
    }

    const onReadyToPlay = async () =>{
        const reqBody = {
            gameID:uuid.v4(),
            adminName:'test',
            playersName:[...playersName],
            oauthToken:uuid.v4()
        }

        const config = {
            headers:{
                'Content-Type':'application/json'
            }
        }

        try {
            const res = await axios.post('http://192.168.0.85:5000/api/v1/storeGameInfo',JSON.stringify(reqBody),config)
            if(res.status===200){
                navigation.navigate('Game',{ gameId: reqBody.gameID })
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <DismissalKeyboard>
            <SafeAreaView style = {styles.main_screen_container}>
                <View style = {styles.main_screen_padding}>
                    <View style = {styles.num_player_container}>
                        <Text style = {styles.num_of_player_text}> Enter Number of Player</Text>
                        <TextInput
                            keyboardType='numeric'
                            style={styles.input}
                            placeholder='Enter # of player'
                            placeholderTextColor="#aaaaaa"
                            onChangeText={(text) => onSetNumOfPlayer(text)}
                            value={numOfPlayer}
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                        />
                    </View>
                    {numValue > 1 && 
                        <>
                            <View style = {styles.player_name_container}>
                                <Text style = {styles.player_name_text}> Enter Players name</Text>
                                <KeyboardAwareScrollView style={{ height: '100%',width:'100%' }} keyboardShouldPersistTaps="always">
                                    {
                                        createDynamicInput()
                                    }
                                </KeyboardAwareScrollView>
                            </View>
                            <View style = {styles.ready_button_container}>
                                <TouchableHighlight onPress={()=>onReadyToPlay()}>
                                    <View style = {styles.login_button_container}>
                                        <Ionicons style = {styles.login_button} name="game-controller-outline" size={32} />
                                        <Text style = {styles.login_button_text} accessibilityLabel="Learn more about this purple button"> Ready To Play !!! </Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </>
                    }
                </View>
            </SafeAreaView>
        </DismissalKeyboard>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    main_screen_container:{
        flex: 1,
        backgroundColor: '#008080'
    },
    main_screen_padding:{
        flex:1,
        padding: 20
    },
    num_player_container:{
        flex:2
    },
    num_of_player_text:{
        fontSize: 20,
        paddingBottom: 15
    },
    player_name_container:{
        flex:10
    },
    player_name_text:{
        fontSize: 20,
        paddingTop: 15
    },
    input: {
        height: 48,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 16
    },
    login_button:{
        padding:5
    },
    login_button_text:{
        fontSize:15,
        fontWeight:'bold',
    },
    login_button_container:{
        flexDirection:'row',
        borderWidth: 2,
        borderColor: 'black',
        borderStyle: 'solid',
        borderRadius: 50,
        alignItems:'center',
        justifyContent:'center'
    },
    ready_button_container:{
        flex:1
    },
    app_version_container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})