import React,{useState} from 'react';
import { Image, StyleSheet, Text, View, TouchableHighlight, TextInput, SafeAreaView } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { signInWithEmailAndPassword } from '../firebase.util';

const LoginScreen = ():JSX.Element =>{
    const [userEmail, setUserEmail] = useState("");
    const [userPassword, setUserEmailPassoword] = useState("");

    const onLogin = async (): Promise<void> =>{
        try {
            const response = await signInWithEmailAndPassword(userEmail,userPassword);
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <SafeAreaView style = {styles.main_screen_container}>
            <LinearGradient style = {styles.background} colors ={['#96e4df','#008080']}/>
            <KeyboardAwareScrollView style={{ height: '100%',width:'100%' }} keyboardShouldPersistTaps="always">
                <View style = {styles.login_screen_container}>
                    <View style = {styles.image_container}>
                        <Image style={styles.main_image} source={require("../assets/loginScreen.png")} />
                    </View>
                    <View style = {styles.credential_container}>
                        <Text style = {styles.welcome_text}> Welcome !!!</Text>
                        <TextInput
                            style={styles.input}
                            placeholder='Gmail'
                            placeholderTextColor="#aaaaaa"
                            onChangeText={(text) => setUserEmail(text)}
                            value={userEmail}
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                        />
                        <TextInput
                            style={[styles.input,{marginBottom:20}]}
                            placeholderTextColor="#aaaaaa"
                            secureTextEntry
                            placeholder='Password'
                            onChangeText={(text) => setUserEmailPassoword(text)}
                            value={userPassword}
                            underlineColorAndroid="transparent"
                            autoCapitalize="none"
                        />
                        <TouchableHighlight onPress={()=>onLogin()}>
                            <View style = {styles.login_button_container}>
                                <Ionicons style = {styles.login_button} name="logo-google" size={32} />
                                <Text style = {styles.login_button_text} accessibilityLabel="Learn more about this purple button"> Google LogIn </Text>
                            </View>
                        </TouchableHighlight>
                        <View style = {styles.login_text}>
                            <Text> Login or Sign up</Text>
                        </View>
                        <View style = {styles.app_version_container}>
                            <Text >
                                app version 1.0.0
                            </Text>
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    main_screen_container:{
        flex: 1,
        backgroundColor: '#008080'
    },
    login_screen_container:{
        padding: 20,
        height:'160%'
    },
    input: {
        height: 48,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 16
    },
    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: '100%',
    },
    image_container: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10
    },
    main_image: {
        width: 225,
        height: 225
    },
    credential_container:{
        flex: 3
    },
    welcome_text:{
        fontSize: 30,
        paddingBottom: 15
    },
    login_button_container:{
        flexDirection:'row',
        borderWidth: 2,
        borderColor: 'black',
        borderStyle: 'solid',
        borderRadius: 50,
        alignItems:'center',
        justifyContent:'center'
    },
    login_button:{
        padding:5
    },
    login_button_text:{
        fontSize:15,
        fontWeight:'bold',
    },
    login_text:{
        fontSize: 15,
        paddingLeft: 5,
        paddingTop:10,
        flex:1,
        alignItems:'center'
    },
    app_version_container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
});