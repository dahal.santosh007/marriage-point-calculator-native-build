import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

import getEnvVars from './environment';

const { firebaseCredential } = getEnvVars();

const REACT_APP_FIREBASECONFIG = {
    apiKey: firebaseCredential.firebaseApiKey ,
    authDomain: firebaseCredential.firebaseAuthDomain,
    databaseURL: firebaseCredential.firebaseDatabaseURL,
    projectId: firebaseCredential.firebaseProjectId,
    storageBucket: firebaseCredential.firebaseStorageBucket,
    messagingSenderId:firebaseCredential.firebaseMessagingSenderId,
    appId: firebaseCredential.firebaseAppId
};

firebase.initializeApp(REACT_APP_FIREBASECONFIG);

export const auth = firebase.auth();

export const db = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({ prompt: 'select_account' });

export const signInWithEmailAndPassword = (email,password) => auth.signInWithEmailAndPassword(email,password);

export default firebase;
