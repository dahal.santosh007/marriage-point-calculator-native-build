import 'react-native-gesture-handler';
import React, { useState,useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { LoginScreen ,HomeScreen } from './screen';
import { auth } from './firebase.util';

const Stack = createStackNavigator();

export default function App() {
  const [user, setUser] = useState(null)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user:any) => {
      if (user) {
        setUser(user);
      } else {
        setLoading(false)
      }
    });
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        { user ? (
          <Stack.Screen name="Home">
            {props => <HomeScreen/>}
          </Stack.Screen>
        ) : (
          <>
            <Stack.Screen name="test" component={LoginScreen} options={{ headerShown: false }}/>
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
